/*
 * tokenizer.h
 *
 *  Created on: 20.01.2014
 *      Author: david
 */

#ifndef __MYPARSERLIB_TOKENIZER_H_
#define __MYPARSERLIB_TOKENIZER_H_

#include "core.h"

#include <cassert>
#include <initializer_list>
#include <memory>
#include <ostream>
#include <set>
#include <string>

namespace MyParserLib {

template<typename CharT, typename TagT>
class Token {
public:
	typedef CharT part_type;
	typedef typename std::basic_string<CharT> content_type;
	typedef TagT tag_type;

	Token() :
			begin(-1) {
	}

	Token(TagT _tag, const std::basic_string<CharT>& _content, unsigned int _begin) :
			tag(_tag), begin(_begin) {
		content.assign(_content);
	}

	Token(const Token& other) :
			tag(other.tag), begin(other.begin) {
		content.assign(other.content);
	}

	TagT getTag() const {
		return tag;
	}

	unsigned int getBegin() const {
		return begin;
	}

	const std::basic_string<CharT>& getContent() const {
		return content;
	}

private:
	TagT tag;
	std::basic_string<CharT> content;
	unsigned int begin;
};

template<typename CharT, typename TagT>
std::ostream& operator<<(std::ostream& stream, const Token<CharT, TagT>& token) {
	stream << "Token(" << token.getTag() << "), \"" << token.getContent() << "\")";
	return stream;
}

template<typename TokenIterator>
class MatchToken;

template<typename Iterator, typename TagT>
class Tokenizer {
public:
	typedef typename Iterator::value_type part_type;
	typedef Token<part_type, TagT> token_type;
	typedef typename token_type::content_type content_type;
	typedef Parser<Iterator, content_type> TokenParser;

	/**
	 * Adds a rule to the Tokenizer. They will be tried in order they were added,
	 * and the resulting token receives the specified
	 */
	void addRule(std::shared_ptr<TokenParser> parser, TagT tag) {
		rules.push_back(make_pair(parser, tag));
	}

	void addRule(std::shared_ptr<Parser<Iterator, typename Iterator::value_type>> parser, TagT tag) {
		auto singletonToString = [](part_type c) {
			return content_type(1, c);
		};

		rules.push_back(make_pair(apply(parser, singletonToString), tag));
	}

	void addRule(std::shared_ptr<Parser<Iterator, Void>> parser, TagT tag) {
		auto makeEmptyString = [](Void) {
			return content_type();
		};

		rules.push_back(make_pair(apply(parser, makeEmptyString), tag));
	}

	bool getTokens(Iterator begin, Iterator end, typename std::vector<token_type>& tokens) const {
		ParserState<Iterator> state(begin, end);

		while (!state.atEndOfInput()) {
			std::basic_string<part_type> token;
			bool parsed = false;

			for (unsigned int i = 0; !parsed && i < rules.size(); ++i) {
				unsigned position = state.getIndex();

				try {
					ParserState<Iterator> copy(state);
					// This call also advances the input position.
					rules[i].first->parse(copy, token);
					tokens.emplace_back(rules[i].second, token, position);
					state = copy;
					parsed = true;
					break;
				} catch (ParseException&) {
				}
			}

			if (!parsed) {
				return false;
			}
		}

		return true;
	}

private:
	std::vector<std::pair<std::shared_ptr<TokenParser>, TagT>> rules;
};

template<typename TokenIterator>
class MatchToken: public Parser<TokenIterator, typename TokenIterator::value_type> {
public:
	typedef typename TokenIterator::value_type token_type;

	MatchToken(typename TokenIterator::value_type::tag_type _expectedTag) {
		expectedTags.insert(_expectedTag);
	}

	MatchToken(std::initializer_list<typename TokenIterator::value_type::tag_type> tags) :
			expectedTags(tags.begin(), tags.end()) {
	}

	virtual ~MatchToken() {
	}

	virtual void parse(ParserState<TokenIterator>& state, typename TokenIterator::value_type& out) {
		if (state.atEndOfInput()) {
			throw ParseException(state.getIndex(), "Unexpected end of input");
		}

		if (expectedTags.find(state.getPos()->getTag()) == expectedTags.end()) {
			throw ParseException(state.getIndex(), "Unexpected token");
		}

		out = *state.getPos();
		++state.getPos();
	}

private:
	std::set<typename TokenIterator::value_type::tag_type> expectedTags;
};

template<typename TokenIterator>
typename std::shared_ptr<Parser<TokenIterator, typename TokenIterator::value_type>> token(
		typename TokenIterator::value_type::tag_type tag) {
	return std::make_shared < MatchToken < TokenIterator >> (tag);
}

template<typename TokenIterator>
typename std::shared_ptr<Parser<TokenIterator, typename TokenIterator::value_type>> token(
		std::initializer_list<typename TokenIterator::value_type::tag_type> tags) {
	return std::make_shared < MatchToken < TokenIterator >> (tags);
}

}

#endif /* __MYPARSERLIB_TOKENIZER_H_ */
